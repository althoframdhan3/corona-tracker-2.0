from django.http import response
from django.http.request import HttpRequest
from django.test import TestCase
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from .views import *
from .models import *
from .apps import ForumsConfig
from userProfile.models import Account

# Create your tests here.


class TestForum(TestCase):

    def test_url_forum_page(self):
        response = self.client.get(reverse('forums:forums'))
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(ForumsConfig.name, 'forums')
    
    def test_try_add_forum_before_login(self):
        response = self.client.get(reverse('forums:forums_add_forum'))
        self.assertContains(response, "You must be logged in before creating a new forum")
    
    def dummyUser(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        akun = Account.objects.create(
            user = user,
			name = user.username,
			email = user.email,
			job = "petani",
			age = 15,
        )
        user.set_password("12345")
        user.save()

        self.client.login(
            username="dummy",
            password="12345"
        )
    
    def test_add_forum_GET(self):
        self.dummyUser()
        response = self.client.get(reverse('forums:forums_add_forum'))
        self.assertTemplateUsed(response, 'forums/add_forum.html')
    
    def test_add_forum_POST(self):
        self.dummyUser()
        response = self.client.post(
            reverse('forums:forums_add_forum'),
            data={
                'caption':'Test Cap',
                'body':'Test Body'
            })
        
        self.assertEqual(Forum.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)

    
    def dummyForum(self):
        self.dummyUser()
        pk = Forum.objects.create(
            caption="Test Cap",
            body="Test Body",
            author=Account.objects.get(name="dummy")
        )
        return str(pk.id)

    def test_detail_forum_not_author_GET(self):
        pk = self.dummyForum()
        self.client.logout()
        response = self.client.get(
            reverse('forums:forums_detail_forum', args=pk)
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Cap")
        self.assertNotContains(response, "delete_forum")

    def test_detail_forum_author_GET(self):
        pk = self.dummyForum()
        response = self.client.get(
            reverse('forums:forums_detail_forum', args=pk)
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Cap")
        self.assertContains(response, "delete_forum")
    
    def test_detail_forum_comment_POST(self):
        pk = self.dummyForum()
        response = self.client.post(
            reverse('forums:forums_detail_forum', args=pk),
            data={
                "body":"Test Comment",
                "forum":pk,
            }
        )

        self.assertEqual(Comment.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)

        response = self.client.get(
            reverse('forums:forums_detail_forum', args=pk)
        )

        self.assertContains(response, "Test Comment")
    
    def test_delete_forum_not_author_POST(self):
        pk = self.dummyForum()
        self.client.logout()
        response = self.client.post(
            reverse('forums:forums_delete_forum', args=pk)
        )
        self.assertNotEqual(Forum.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)
    
    def test_delete_forum_author_POST(self):
        pk = self.dummyForum()
        response = self.client.post(
            reverse('forums:forums_delete_forum', args=pk)
        )
        self.assertEqual(Forum.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)
    
    def test_search_found(self):
        self.dummyForum()
        url = reverse("forums:forums_search")
        response = self.client.get(url, {'search':'Test'})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test")

    def test_search_not_found(self):
        self.dummyForum()
        url = reverse("forums:forums_search")
        response = self.client.get(url, {'search':'Kenta'})
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Kenta")

    def test_follow_forum(self):
        self.dummyForum()
        url = reverse("forums:forums_set_follow")
        response = self.client.get(url, {'id':'1', 'action':'add'})
        self.assertEqual(response.status_code, 200)
        url = reverse("forums:forums_get_follow")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Cap")



    def test_unfollow_forum(self):
        self.dummyForum()
        url = reverse("forums:forums_set_follow")
        response = self.client.get(url, {'id':'1', 'action':'add'})
        self.assertEqual(response.status_code, 200)
        url = reverse("forums:forums_get_follow")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Cap")
        url = reverse("forums:forums_set_follow")
        response = self.client.get(url, {'id':'1', 'action':'remove'})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Cap")
        url = reverse("forums:forums_get_follow")
        response = self.client.get(url)
        self.assertNotContains(response, "Test Cap")

    