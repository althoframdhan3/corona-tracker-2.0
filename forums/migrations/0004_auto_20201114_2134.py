# Generated by Django 3.1.1 on 2020-11-14 14:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forums', '0003_auto_20201114_1811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forum',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
