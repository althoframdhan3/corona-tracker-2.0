from django import forms
from django.forms.widgets import Textarea
from .models import Forum, Comment

class ForumForm(forms.ModelForm):
    
    class Meta:
        model = Forum
        exclude = ['author','followed_by']
        widgets = {
            'caption': Textarea(
                attrs={
                    'placeholder' : 'Write a caption...',
                    'class'       : 'materialize-textarea'
                    }),
            'body'  : Textarea(
                attrs={
                    'class'       : 'materialize-textarea',
                    'id'          : 'textarea1'
                    }),
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ['author']
        widgets = {
            'body'  : Textarea(
                attrs={
                    'placeholder' : 'Add a comment...',
                    'class'       : 'materialize-textarea',
                    }),
        }