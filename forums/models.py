from django.db import models
from userProfile.models import Account
from django.db.models.deletion import CASCADE

class Forum(models.Model):
    caption = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    author = models.ForeignKey(Account, default=None, on_delete=CASCADE, related_name="+")

    followed_by = models.ManyToManyField(Account, default=None)
    class Meta:
        ordering = ['-timestamp']

class Comment(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    forum = models.ForeignKey(Forum, related_name="comments", on_delete=CASCADE)
    author = models.ForeignKey(Account,  default=None, on_delete=CASCADE)