from userProfile.models import Account
from django.core.serializers import serialize
from django.db.models.query_utils import Q
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from .models import Forum
from .forms import ForumForm, CommentForm
import json


def forums(request, warn=False):
    request.session['forums_item'] = Forum.objects.all().count()
    context = {
        'forums': Forum.objects.all(),
        'warn': warn
    }
    return render(request, 'forums/forums.html', context)


def add_forum(request):
    if not request.user.is_authenticated:
        return forums(request, warn=True)

    form = ForumForm()
    if request.method == "POST":
        form = ForumForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user.account
            instance.save()
            return redirect('forums:forums')

    context = {
        "form": form
    }
    return render(request, 'forums/add_forum.html', context)


def detail_forum(request, pk):
    forum = Forum.objects.get(id=pk)
    comment_form = CommentForm()
    context = {
        "forum": forum,
        "author": True,
        "warn": False
    }

    if request.user != forum.author.user:
        context['author'] = False

    if not request.user.is_authenticated:
        context['warn'] = True
        return render(request, 'forums/detail_forum.html', context)

    if request.method == "POST":
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            instance = comment_form.save(commit=False)
            instance.author = request.user.account
            instance.save()
            return redirect('forums:forums_detail_forum', pk)

    context['comment_form'] = comment_form
    return render(request, 'forums/detail_forum.html', context)


def delete_forum(request, pk):
    forum = Forum.objects.get(id=pk)
    if request.user != forum.author.user:
        return redirect('forums:forums_detail_forum', pk)
    if request.method == 'POST':
        forum.delete()
    return redirect('forums:forums')


def ajax_search(request):
    query = request.GET.get("search")

    forums = Forum.objects.filter(
        Q(caption__icontains=query) |
        Q(body__icontains=query))

    json_dump = json.loads(serialize('json', forums))
    updated_json = []
    for card in json_dump:
        author = Account.objects.get(id=card['fields']['author'])
        author_image = author.image
        author_name = author.name
        author_job = author.job
        id = card['pk']
        comments = Forum.objects.get(id=id).comments.count()
        card['fields']['author_image'] = author_image
        card['fields']['author_name'] = author_name
        card['fields']['author_job'] = author_job
        card['fields']['comments'] = comments
        updated_json.append(card)

    return JsonResponse(updated_json, safe=False)


def ajax_set_follow(request):
    pk = request.GET.get("id")

    forum = Forum.objects.get(id=pk)
    user = request.user.account

    action = request.GET.get("action")
    if action == 'add':
        forum.followed_by.add(user)
    else:
        forum.followed_by.remove(user)

    return JsonResponse(json.loads(serialize('json', Forum.objects.all())), safe=False)


def ajax_get_follow(request):

    user = request.user.account
    followed = user.forum_set.all()

    json_dump = json.loads(serialize('json', followed))
    updated_json = []
    for card in json_dump:
        author = Account.objects.get(id=card['fields']['author'])
        author_image = author.image
        author_name = author.name
        author_job = author.job
        id = card['pk']
        comments = Forum.objects.get(id=id).comments.count()
        card['fields']['author_image'] = author_image
        card['fields']['author_name'] = author_name
        card['fields']['author_job'] = author_job
        card['fields']['comments'] = comments
        updated_json.append(card)

    return JsonResponse(updated_json, safe=False)
