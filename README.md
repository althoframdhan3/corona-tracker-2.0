[![pipeline status](https://gitlab.com/althoframdhan3/corona-tracker-2.0/badges/master/pipeline.svg)](https://gitlab.com/JOSHUARIHI/ppwcorona/-/commits/master)
[![coverage report](https://gitlab.com/althoframdhan3/corona-tracker-2.0/badges/master/coverage.svg)](https://gitlab.com/JOSHUARIHI/ppwcorona/-/commits/master)

# CoronaTrackerLite
## Nama Anggota :
- Joshua Diogenes Samuel Rihi (1906353486)
- Muhammad Kenta Bisma Dewa Brata (1906350950)
- Althof Rafaella Ramdhan (1906350710)
- Muhammad Anis Abdul Aziz (1906353492)

## Link HerokuApp
[Link HerokuApp](https://covidtracker-2.herokuapp.com/)

## Project Description

CoronaTrackerLite adalah sebuah aplikasi berbasis website yang menyediakan informasi seputar perkembangan penyakit COVID-19 di Indonesia. fitur utama yang kita bawakan adalah Blog yang akan didominasi oleh artikel yang membahas tips-tips seputar perlindungan diri terhadap virus Corona agar masyarakat luas dapat tersosialisasikan dengan cepat dan tepat. pada aplikasi ini kami mengusung beberapa fitur lainnya yaitu:
  - Login<br>
    - User bisa login
  - Register<br>
    - User bisa Register
  - Forums<br>
    - User bisa comment pada satu Forums
    - User bisa nge-post suatu tulisan ke Forums
  - COVID News<br>
    - User dapat melihat berita COVID-19 di Indonesia
  - angka prevalensi COVID-19
    - User dapat melihat jumlah kematian di Indonesia
    - User dapat melihat jumlah sembuh di Indonesia
    - User dapat melihat jumlah pasien terjangkit COVID-19 di Indonesia