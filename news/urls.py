from django.urls import path

from . import views

app_name = 'news'

urlpatterns = [
    path('', views.news, name='news'),
    path('<int:news_id>/<str:news_title>/', views.news_detail, name='news_detail'),
    path("search/", views.news_search_result, name="ajax_news"),
    path("likes/add/", views.add_likes, name="add_likes"),
    path("likes/remove/", views.remove_likes, name="remove_likes"),
    path("bookmarked/", views.ajax_bookmark, name="ajax_bookmark"),
    path("bookmark/remove/", views.remove_bookmark, name="API_remove_bookmark"),
    path("bookmark/add/", views.add_bookmark, name="API_add_bookmark")
]