from datetime import datetime
from logging import DEBUG
from django.http.request import HttpRequest
from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils.dateparse import parse_datetime
from .views import *
from .models import *
from .apps import NewsConfig
from corona_app.settings import API_KEY
from .utils import retrieve_API


# Create your tests here.


class TestNews(TestCase):

    def setUp(self):
        News.objects.create(
            title='berita baru'
        )

    def test_apps(self):
        self.assertEqual(NewsConfig.name, 'news')

    def test_news_detail_view(self):
        response = self.client.get(
            reverse('news:news_detail', args=[1,'berita baru'])
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(News.objects.all().count(), 1)

    def test_news_fetch_API(self):
        response = self.client.get(
            reverse('news:news')
        )
        self.assertEqual(response.status_code, 200)

    def test_models_toString(self):
        self.assertEqual(str(News.objects.get(id=1)), "berita baru")

    def dummyUser(self):
        from django.contrib.auth.models import User
        from userProfile.models import Account
        user = User.objects.create(username="dummy", email="dummy@example.com")
        akun = Account.objects.create(
            user = user,
			name = user.username,
			email = user.email,
			job = "petani",
			age = 15,
        )
        user.set_password("12345")
        user.save()

        self.client.login(
            username="dummy",
            password="12345"
        )

    def test_ajax_search_result(self):
        url = reverse('news:ajax_news')
        response = self.client.get(url, {'search': "baru", 'pageNow':1})
        self.assertContains(response, "baru")

    def test_add_likes(self):
        self.dummyUser()
        news = News.objects.create(
            title='berita baru'
        )
        
        url = reverse('news:add_likes')
        self.assertEqual(len(news.likedby.all()), 0)

        self.client.get(url, {'id': news.id})
        self.assertEqual(len(news.likedby.all()), 1)

    def test_remove_likes(self):
        self.dummyUser()
        news = News.objects.create(
            title='berita baru'
        )
        
        url_add = reverse('news:add_likes')
        self.client.get(url_add,{'id': news.id})
        self.assertEqual(len(news.likedby.all()), 1)

        url_remove = reverse('news:remove_likes')
        self.client.get(url_remove, {'id': news.id})
        self.assertEqual(len(news.likedby.all()), 0)

    def test_remove_bookmark(self):
        self.dummyUser()
        news = News.objects.create(
            title='berita baru'
        )
        
        url_add = reverse('news:API_add_bookmark')
        self.client.get(url_add,{'id': news.id})
        self.assertEqual(len(news.bookmarkedby.all()), 1)

        url_remove = reverse('news:API_remove_bookmark')
        self.client.get(url_remove, {'id':news.id})
        self.assertEqual(len(news.bookmarkedby.all()), 0)

    def test_add_bookmark(self):
        self.dummyUser()
        news = News.objects.create(
            title='berita baru'
        )
        
        url = reverse('news:API_add_bookmark')
        self.assertEqual(len(news.bookmarkedby.all()), 0)

        response = self.client.get(url,{'id': news.id})
        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(len(news.bookmarkedby.all()), 1)
