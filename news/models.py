from corona_app.settings import TIME_ZONE
from django.db import models
from django.utils import timezone
from userProfile.models import Account
import pytz
# Create your models here.

class News(models.Model):
    title = models.CharField(max_length=200, null=True)
    name = models.CharField(max_length=200, null=True)
    author = models.CharField(max_length=100, null=True)

    description = models.CharField(max_length=400, null=True)
    content = models.CharField(max_length=400, null=True)
    source = models.CharField(max_length=400, null=True)
    image_url = models.CharField(max_length=300, blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
    like = models.PositiveIntegerField(default=0)

    bookmarkedby = models.ManyToManyField(Account)
    likedby = models.ManyToManyField(Account, related_name="likedby")

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return self.title
