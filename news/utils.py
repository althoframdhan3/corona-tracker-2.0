from .models import News

import requests
from corona_app.settings import API_KEY
from requests.exceptions import ConnectionError

from dateutil import parser


def retrieve_API():
    url = 'http://newsapi.org/v2/top-headlines?q=covid&country=id&category=health&apiKey='+API_KEY
    try:
        response = requests.get(url)
    except ConnectionError:
        response = {}
    
    try:
        list_articles = response.json()['articles']
        for news in list_articles:
            # try:
            if news['title'] and news['description'] and news['content'] and news['urlToImage'] != None:
                
                newNews = News(
                    title=news['title'],
                    name=news['source']['name'],
                    author=news['author'],
                    description=news['description'],
                    content=news['content'],
                    source=news['url'],
                    image_url=news['urlToImage'],
                    timestamp=parser.isoparse(news['publishedAt'])
                )
                
                getNews = News.objects.filter(title=newNews.title)
                if len(getNews)==0:
                    try:
                        newNews.save()
                    except:
                        pass
               
    except KeyError as e:
        pass
