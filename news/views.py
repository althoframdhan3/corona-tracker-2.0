from django.contrib.auth.decorators import login_required
from .models import News
from django.shortcuts import render

# Create your views here.
from .utils import retrieve_API
from django.db.models import Q
from django.http import JsonResponse
from django.core.serializers import serialize
import json, math
from django.shortcuts import redirect, render
from userProfile.models import Account


def news(request):

    retrieve_API()
    context = {'user_id':0}
    if request.user.is_authenticated:
        context['user_id'] = request.user.account.id

    return render(request, 'news/news.html', context)

def news_detail(request, news_id, news_title):
    obj = News.objects.get(pk=news_id)
    context = {
        'news': obj
    }
    return render(request, 'news/news_detail.html', context)

def news_search_result(request):
    
    keyword = request.GET.get('search')
    pageNow = int(request.GET.get('pageNow'))
    awal = pageNow*10-10
    akhir = awal + 10
    
    articles = News.objects\
        .filter(Q(title__icontains=keyword) | Q(content__icontains=keyword))
    articles_pagination = articles[awal:akhir]

    responseData = serialize('json', articles_pagination)
    responseData = json.loads(responseData)

    page_length = math.ceil(len(articles)/10)

    data_news = {
        'data': responseData, 
        'page_length':page_length, 
        'item_found':len(articles)
        }
    
    return JsonResponse(data_news, safe=False)

def add_likes(request):
    idnya = (request.GET.get('id'))
    news_obj = News.objects.get(pk=idnya)
    
    news_obj.likedby.add(request.user.account)

    responseData = serialize('json', News.objects.filter(pk=idnya))
    responseData = json.loads(responseData)

    return JsonResponse(responseData, safe=False)

def remove_likes(request):
    idnya = (request.GET.get('id'))
    news_obj = News.objects.get(pk=idnya)
    
    news_obj.likedby.remove(request.user.account)

    responseData = serialize('json', News.objects.filter(pk=idnya))
    responseData = json.loads(responseData)

    return JsonResponse(responseData, safe=False)

@login_required(login_url='/userProfile/login')
def ajax_bookmark(request):
    
    user = request.user.account
    all_bookmarked = user.news_set.all()

    responseData = serialize('json', all_bookmarked)
    responseData = json.loads(responseData)
    return JsonResponse(responseData, safe=False)

def remove_bookmark(request):
    
    idnya = (request.GET.get('id'))
    user = request.user.account
    news = News.objects.filter(id=idnya)
    
    news[0].bookmarkedby.remove(user)

    responseData = serialize('json', news)
    responseData = json.loads(responseData)
    return JsonResponse(responseData, safe=False)

def add_bookmark(request):
    
    idnya = (request.GET.get('id'))
    user = request.user.account
    news = News.objects.filter(id=idnya)
    
    news[0].bookmarkedby.add(user)
    
    responseData = serialize('json', news)
    responseData = json.loads(responseData)
    return JsonResponse(responseData, safe=False)

