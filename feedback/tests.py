from django.test import TestCase,Client
from django.http.request import HttpRequest
from django.urls import reverse, resolve
from django.contrib.auth import get_user_model
from django.contrib.auth import login, authenticate, logout

# Create your tests here.
from .models import Feedback, Rate
from django.contrib.auth.models import User
from . import views
from userProfile.models import Account


# Create your tests here.
class feedback(TestCase):
    
    def test_home_page(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')
        
    def test_func_home(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, views.home)
        
    def test_model_feedback(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        akun = Account.objects.create(
            user = user,
			name = user.username,
			email = user.email,
			job = "petani",
			age = 15,
        )
        user.set_password("12345")
        user.save()

        self.client.login(
            username="dummy",
            password="12345"
        )
        Rate.objects.create(author = user, score = 2, nama = user.username)
        Feedback.objects.create(
            body='HALO',
            author = akun,
            topik = "other",
            nilai = 4,
        )     
        response = self.client.post(
            reverse('feedback:olah'),
            data={
                'val': 3,
            }
        )
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual((Rate.objects.all().count()), 1)
        self.assertJSONEqual(response.content, {'success':'true', 'score': '3'})
        
        hasil = Feedback.objects.get(body='HALO')
        rate = Rate.objects.get(nama = 'dummy')
        self.assertEqual(str(hasil), 'HALO')
        self.assertEqual(hasil.nilai, 4)
        self.assertEqual(str(rate), 'dummy')
    
        
    def test_try_add_feedback_before_login(self):
        response = self.client.get(reverse('feedback:feedback'))
        self.assertContains(response, "You must be logged in before posting a feedback")

    def dummy_user(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        akun = Account.objects.create(
            user = user,
			name = user.username,
			email = user.email,
			job = "petani",
			age = 15,
        )
        user.set_password("12345")
        user.save()

        self.client.login(
            username="dummy",
            password="12345"
        )
        
    def test_add_feedback_get(self):
        self.dummy_user();
        response = self.client.get(reverse('feedback:feedback'))
        self.assertTemplateUsed(response, 'feedback.html')
        
    def test_add_feedback_post(self):
        self.dummy_user();
        response = self.client.post(
            reverse('feedback:feedback'),
            data= {'body' : 'haiii papa kabar'}
            )
        
        self.assertEqual(Feedback.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)
        response =  Feedback.objects.get(body='haiii papa kabar')
        self.assertEqual(str(response), "haiii papa kabar")


    def test_rate(self):
        response = Client().get('/feedback/rate/')
        self.assertEqual(response.status_code, 200)
        
    def test_rate_func(self):
        response = resolve('/feedback/rate/')
        self.assertEqual(response.func, views.olah)
        
    def test_rate_ajax_yangbelumada(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        user.set_password("12345")
        user.save()
        self.client.login(
            username="dummy",
            password="12345"
        )
        response = self.client.post(
            reverse('feedback:olah'),
            data={
                'val': 3,
            }
        )
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual((Rate.objects.all().count()), 1)
        self.assertJSONEqual(response.content, {'success':'true', 'score': '3'})
    
    def test_rate_ajax_yangsudahada(self):   
        user = User.objects.create(username="dummy", email="dummy@example.com")
        user.set_password("12345")
        user.save()
        self.client.login(
            username="dummy",
            password="12345"
        )
        Rate.objects.create(author = user, score = 2, nama = user.username)
        response = self.client.post(
            reverse('feedback:olah'),
            data={
                'val': 3,
            }
        )
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual((Rate.objects.all().count()), 1)
        self.assertJSONEqual(response.content, {'success':'true', 'score': '3'})
    

        

        

  