from django.conf import settings
from django.db import models
from userProfile.models import Account

from django.db.models.deletion import CASCADE
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Feedback(models.Model):
    body = models.TextField()
    waktu = models.DateTimeField(auto_now_add=True, blank=True)
    topik = models.CharField(max_length=150, null=True)
    nama = models.CharField(max_length=150, null=True)
    author = models.ForeignKey(Account, default=None, on_delete=models.CASCADE)
    nilai = models.IntegerField(default = 0,
            validators = [
                MaxValueValidator(5),
                MinValueValidator(0),
            ]
            )
    class Meta:
        ordering = ['-waktu']
    def __str__(self):
        return self.body
    
class Rate(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    nama = models.CharField(max_length=150, null=True)
    score = models.IntegerField(default = 0,
            validators = [
                MaxValueValidator(5),
                MinValueValidator(0),
            ]
            )
    def __str__(self):
        return self.author.username