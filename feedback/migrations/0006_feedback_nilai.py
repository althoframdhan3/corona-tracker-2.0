# Generated by Django 3.1.1 on 2020-12-28 08:40

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0005_feedback_topik'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='nilai',
            field=models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(5), django.core.validators.MinValueValidator(0)]),
        ),
    ]
