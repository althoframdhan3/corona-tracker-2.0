# Generated by Django 3.1.1 on 2020-12-26 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0003_rate'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='nama',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
