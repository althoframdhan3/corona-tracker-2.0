from django.shortcuts import render,redirect
from userProfile.models import User
from .forms import feedback
from .models import Feedback, Rate
from django.http import JsonResponse

# Create your views here.
def belum_login(request, warn = False):
    hasil =  feedback()
    context = {
        'form' : hasil,
        'warn' : warn,
    }
    return render(request, 'feedback.html', context)
   
def home(request):
    if not request.user.is_authenticated:
        return belum_login(request, True)
    apakah = False
    total = 0
    if request.method == "POST":
        form = feedback(request.POST or None, request.FILES or None)
    
        if form.is_valid():
            apakah = True
            instance = form.save (commit = False)
            topik = request.POST.get("topik")
           
            instance.author = request.user.account
            instance.nama = request.user.username
            instance.topik = topik
            if (Rate.objects.filter(nama = request.user.username).exists()):
                baru = Rate.objects.get(nama = request.user.username)
                instance.nilai = baru.score
            instance.save()
            
            
    form = feedback()   
    total = Feedback.objects.all().count()
    semua = Feedback.objects.all()
    rate = Rate.objects.all()
    context = {
        "formulir"  : form,
        "apakah"    : apakah,
        "total"     : total,
        "semua"     : semua,
        "rate"      : rate,
    }
    return render(request, 'feedback.html', context)


def olah (request):
    if request.method == 'POST':
        val = request.POST.get('val')
        penulis = request.user
        obj = Rate.objects.filter(nama = penulis.username).exists()
        if (obj):
            baru = Rate.objects.get(nama = penulis.username)
            baru.score = val
            baru.save()
            
        else :
            Rate.objects.create(author = penulis, nama = penulis.username, score = val)
           
        return JsonResponse({'success':'true', 'score': val}, safe=False)
    return JsonResponse({'success':'false'})
