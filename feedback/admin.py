from django.contrib import admin
from .models import Feedback, Rate

# Register your models here.
admin.site.register(Feedback)
admin.site.register(Rate)