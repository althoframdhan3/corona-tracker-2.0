from django.test import TestCase, Client
from django.http.request import HttpRequest
from django.urls import reverse, resolve
from .apps import UserprofileConfig as upc
from . import views, models
from .views import *
from .models import Account, User
import json

# Create your tests here.

class TestProfile(TestCase):
	def test_apps(self):
		self.assertEqual(upc.name, 'userProfile')
  
	def test_template_signup(self):
		hasil = Client().get('/userProfile/signup/')
		self.assertTemplateUsed(hasil, 'userProfile/signup.html')
  	
	def test_home_page_signup(self):
		hasil = Client().get('/userProfile/signup/')
		self.assertEqual(hasil.status_code, 200)
  
	def test_home_page_login(self):
		hasil = Client().get('/userProfile/login/')
		self.assertEqual(hasil.status_code, 200)
  
	def test_halaman_signup_views(self):
		found = resolve('/userProfile/signup/')
		self.assertEqual(found.func, views.signup_view)
  
	def test_halaman_logout_views(self):
		found = resolve('/userProfile/logout/')
		self.assertEqual(found.func, views.logout_view)
  
	def test_halaman_login_views(self):
		found = resolve('/userProfile/login/')
		self.assertEqual(found.func, views.login_view)

	def test_halaman_profile_views(self):
		found = resolve('/userProfile/profile/')
		self.assertEqual(found.func, views.profile_view)

	def test_ada_link_signup(self):
		hasil = Client().get('/userProfile/login/')
		self.assertContains(hasil,"Sign up")

	def test_ada_link_login(self):
		hasil = Client().get('/userProfile/signup/')
		self.assertContains(hasil, "Login")
	
	def test_model(self):
		hasil = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
		Account.objects.create(
			user = hasil,
			name = hasil.username,
			email = hasil.email,
			job = "petani",
			age = 15,
		)
		hasil2 = Account.objects.get(id=1)
		self.assertEqual(hasil2.job, "petani")
		self.assertEqual(str(hasil2), "john")

	def test_login(self):
		user = User.objects.create_user(username='adolf',email='adolfo@na.zi',password='hailhailhitler')
		response = self.client.post(reverse('userProfile:login'),data={'username':'adolf','password':'hailhailhitler'})
		self.assertEqual(response.status_code,302)

	def test_login_failed(self):
		response = self.client.post(reverse('userProfile:login'),data={'username':'','password':''})
		self.assertContains(response,"Username OR password is incorrect")

	def test_logout(self):
		User.objects.create_user(username='adolf',email='adolfo@na.zi',password='hailhailhitler')
		self.client.login(username='adolf', password='hailhailhitler')
		response = self.client.get(reverse('userProfile:logout'),{})
		self.assertEqual(response.status_code,302)

	def test_signup(self):
		response = self.client.post(reverse('userProfile:signup'),data={
			'username':'mrPutin',
			'email':'vla@di.mir',
			'password1':'vivarusia',
			'password2':'vivarusia',
			'job': 'president',
			'age': 56})
		self.assertEqual(response.status_code,302)
		self.assertEqual(Account.objects.all().count(), 1)

	def test_profile_settings(self):
		user = User.objects.create(username="mrPutin", email='vla@di.mir')
		account = Account.objects.create(
			user = user,
			name = user.username,
			email = user.email,
			job = 'communist',
			age = 56)
		user.set_password('vivarusia')
		user.save()

		self.client.login(username='mrPutin', password='vivarusia')
		# response = self.client.post(reverse('userProfile:profile'), data={
		# 	'name': user.username,
		# 	'email': user.email,
		# 	'job': account.job,
		# 	'age': 2,
		# 	'profilepicture':'',})		
		# self.assertEqual(response.status_code, 200)

		response = self.client.post(reverse('userProfile:profile'), data={
			'name': user.username,
			'email': user.email,
			'job': account.job,
			'age': 2,})
		# result = json.loads(response.content)
		# print(response.context)
		# self.assertContains(response.context['message'], "successfully")
		self.assertEqual(response.status_code, 200)