from django.urls import path
from . import views

app_name = 'userProfile'

urlpatterns = [
	path('signup/',views.signup_view,name="signup"),
	path('logout/',views.logout_view,name="logout"),
	path('login/',views.login_view,name="login"),
	path('profile/',views.profile_view,name="profile"),
	path('picupdate/',views.picupdate,name="picUpdate"),
]