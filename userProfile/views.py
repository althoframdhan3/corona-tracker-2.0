from django.shortcuts import render, redirect
from .forms import SignupForm, AccountForm, ProfileSettingsForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from .models import *
from django.contrib import messages
from django.http import JsonResponse, HttpResponse
import json
# Create your views here.


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        account_form = AccountForm(request.POST)
        if form.is_valid() and account_form.is_valid():
            user = form.save()
            account = account_form.save(commit=False)
            account.user = user
            account.name = user.username
            account.email = user.email
            account.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect('APP1:index')
    else:
        form = SignupForm()
        account_form = AccountForm()

    context = {'form': form, 'account_form': account_form}
    return render(request, 'userProfile/signup.html', context)


@login_required(login_url='/userProfile/login')
def logout_view(request):
    logout(request)
    return redirect('APP1:index')


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('APP1:index')
        else:
            messages.info(request, 'Username OR password is incorrect')
    context = {}
    return render(request, 'userProfile/login.html', context)


@login_required(login_url='/userProfile/login')
# @allowed_users(allowed_roles=['account'])
def profile_view(request):
    account = request.user.account
    form = ProfileSettingsForm(instance=account)

    context = {'form': form, 'account': account}
    return render(request, 'userProfile/profile.html', context)

@login_required(login_url='/userProfile/login')
def picupdate(request):
    account = request.user.account
    form = ProfileSettingsForm(instance=account)

    if request.method == 'POST':
        form = ProfileSettingsForm(
            request.POST, request.FILES, instance=account)
        if form.is_valid():
            ins = form.save()
            if request.FILES.get('profilepicture'):
                import os
                import base64
                from corona_app.settings import MEDIA_ROOT
                file_path = os.path.join(MEDIA_ROOT, str(request.FILES.get('profilepicture')))
                with open(file_path, "rb") as img_file:
                    my_string = "data:image/png;base64,"+str(base64.b64encode(img_file.read()).decode('utf-8'))
                ins.image = my_string
                data = {
                    'message' : 'Your profile picture so cool!😎',
                    'imgLoc' : account.profilepicture.url[16:],
                    'img' : my_string,
                }
                ins.save()
                
                return JsonResponse(data)
            # elif request.POST.get('profilepicture-clear') != None:
            #     imgLoc = '/static/APP1/icon/profile_default.png'
            #     ins.image = imgLoc
            #     ins.save()
            #     data = {
            #         'message' : 'Your profile picture so ugly!😈',
            #         'img' : imgLoc
            #     }
            #     return HttpResponse(json.dumps(data))
            else:
                data = {
                    'message' : 'Profile updated successfully!😊',
                    'img' : account.image,
                    'imgLoc' : account.profilepicture.url[16:],
                }
                return JsonResponse(data)