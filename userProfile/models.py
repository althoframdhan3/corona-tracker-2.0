from os import truncate
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MinValueValidator

# Create your models here.

class Account(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=150, null=True)
	email = models.EmailField(max_length=200, null=True)
	job	 = models.CharField(max_length=50, null=True)
	age  = models.PositiveIntegerField(validators=[MinValueValidator(1)])
	image = models.TextField(default='/static/APP1/icon/profile_default.png', null=True, blank=True)
	profilepicture = models.ImageField(default='profile_default.png', null=True, blank=True)

	def __str__(self):
		return self.user.username
	

	