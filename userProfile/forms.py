from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Account
from django.utils.translation import gettext_lazy as _

class SignupForm(UserCreationForm):
	class Meta(UserCreationForm.Meta):
		fields = UserCreationForm.Meta.fields + ("email",)
		labels = {
			'email': 'Email',
		}

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ['job','age']

class ProfileSettingsForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = '__all__'
		exclude = ['user', 'image']