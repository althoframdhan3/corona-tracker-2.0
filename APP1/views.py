from django.shortcuts import redirect, render
from news.models import News
from forums.models import Forum
from news.utils import retrieve_API
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from forum.models import Forum
# Create your views here.


def index(request):
    retrieve_API()
    request.session['forums_item'] = Forum.objects.all().count()
    request.session['news_item'] = News.objects.all().count()
    context = {
        'list_news': News.objects.all().order_by('-timestamp')[:6]
    }
    return render(request, 'APP1/landing.html', context)