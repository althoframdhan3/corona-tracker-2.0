from django.http.request import HttpRequest
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from news.models import *
from news.utils import *
from .apps import App1Config

# Create your tests here.


class TestApp(TestCase):

    def test_url_landing_page(self):
        response = self.client.get(reverse('APP1:index'))
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(App1Config.name, 'APP1')

